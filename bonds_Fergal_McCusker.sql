-- Q1. Show all information about the bond with the CUSIP '28717RH95'.
--SELECT * 
--FROM bond
--WHERE CUSIP = '28717RH95'

--Q2. Show all information about all bonds, in order from shortest (earliest maturity) 
-- o longest (latest maturity).
--SELECT * 
--FROM bond
--BY maturity

--Q3. Calculate the value of this bond portfolio, as the sum of the product 
--of each bond's quantity and price.

--SELECT SUM (quantity * price) AS total_portfolio_value
--FROM bond

--Q4. Show the annual return for each bond, as the product of the quantity and 
--the coupon. Note that the coupon rates are quoted as whole percentage points, 
--so to use them here you will divide their values by 100.
--SELECT CUSIP, (quantity * (coupon/100)) 
--FROM bond

--Q5. Show bonds only of a certain quality and above, for example those bonds 
--with ratings at least AA2. (Don't resort to regular-expression or other 
--string-matching tricks; use the bond-rating ordinal.)

--SELECT *
--FROM bond
-- rating in ('AAA', 'AA1', 'AA2')
--ORDER BY rating

--Q6. Show the average price and coupon rate for all bonds of each bond rating.
--SELECT rating, AVG (price) AS average_price, AVG (coupon/100) AS average_coupon_rate
--FROM bond
--GROUP BY rating

--Q7. Calculate the yield for each bond, as the ratio of coupon to price. 
--Then, identify bonds that we might consider to be overpriced, as those whose 
--yield is less than the expected yield given the rating of the bond.

SELECT ID, CUSIP, r.expected_yield, (coupon/price) --AS act_yield
FROM bond b
 JOIN rating r
 ON r.rating = b.rating
WHERE (coupon/price) < r.expected_yield