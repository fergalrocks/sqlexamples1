SELECT *
FROM trade

SELECT *
FROM trader

SELECT *
FROM position

--Q1. Find all trades by a given trader on a given stock - for example the trader 
--with ID=1 and the ticker 'MRK'. This involves joining from trader to position 
--and then to trades twice, through the opening- and closing-trade FKs.

SELECT t.*
FROM position p
JOIN trade t ON (p.opening_trade_ID = t.ID OR p.closing_trade_ID = t.ID)
WHERE p.trader_ID = 1 AND t.stock = 'AAPL'

